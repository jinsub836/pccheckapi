package com.js.pccheckapi.service;

import com.js.pccheckapi.entity.PcRoomCheck;
import com.js.pccheckapi.model.PcRequest;
import com.js.pccheckapi.model.PcResponse;
import com.js.pccheckapi.model.PcRoomItem;
import com.js.pccheckapi.repository.PcRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PcService {
    private final PcRepository pcRepository;

    public void setPcRoom(PcRequest pcRequest){
     PcRoomCheck addData = new PcRoomCheck();
     addData.setDateMaker(LocalDate.now());
     addData.setComputerNo(pcRequest.getComputerNo());
     addData.setUpdate(pcRequest.getUpdate());
     addData.setFixType(pcRequest.getFixType());
     addData.setPcCheck(pcRequest.getPcCheck());
     addData.setEtcMemo(pcRequest.getEtcMemo());

     pcRepository.save(addData);

    }

    public List<PcRoomItem> getPcRoom(){
        List<PcRoomCheck> originlist = pcRepository.findAll();
        List<PcRoomItem> result = new LinkedList<>();

        for (PcRoomCheck pcRoomCheck : originlist){
            PcRoomItem addItem = new PcRoomItem();
            addItem.setId(pcRoomCheck.getId());
            addItem.setDateMaker(pcRoomCheck.getDateMaker());
            addItem.setComputerNo(pcRoomCheck.getComputerNo());
            addItem.setUpdate(pcRoomCheck.getUpdate());
            addItem.setPcStatusCheck(pcRoomCheck.getPcCheck().getPcStatusCheck());
            addItem.setPcStatus(pcRoomCheck.getPcCheck().getPcStatus());

            result.add(addItem);
        }
        return result;
    }

    public PcResponse getPcInfo(long id){
        PcRoomCheck originData = pcRepository.findById(id).orElseThrow();

        PcResponse response = new PcResponse();
        response.setId(originData.getId());
        response.setDateMaker(originData.getDateMaker());
        response.setComputerNo(originData.getComputerNo());
        response.setUpdate(originData.getUpdate());
        response.setFixType(originData.getFixType().getName());
        response.setPcStatusCheck(originData.getPcCheck().getPcStatusCheck() ? "점검 완료": "점검 진행 안됨");
        response.setPcStatus(originData.getPcCheck().getPcStatus());

        return response;
    }
}
