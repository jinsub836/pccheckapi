package com.js.pccheckapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FixType {
    EXPENDABLES("소모품"),
    COMPONENT("부품"),
    NONE("수리 안함");
    private final String name;
    }
