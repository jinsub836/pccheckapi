package com.js.pccheckapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum PcCheck {

    CHECK_OK_GOOD (true,"좋음"),
    CHECK_OK_BAD  (true,"나쁨"),
    CHECK_NO(false,"모름");

     private final Boolean pcStatusCheck;
     private final String pcStatus;
}
