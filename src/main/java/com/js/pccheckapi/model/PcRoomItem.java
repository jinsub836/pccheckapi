package com.js.pccheckapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class PcRoomItem {
    private Long id;
    private LocalDate dateMaker;
    private Short computerNo;
    private String update;
    private Boolean pcStatusCheck;
    private String pcStatus;
}
