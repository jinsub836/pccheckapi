package com.js.pccheckapi.model;

import com.js.pccheckapi.enums.FixType;
import com.js.pccheckapi.enums.PcCheck;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PcRequest {

    private Short computerNo;

    private String update;

    @Enumerated(value = EnumType.STRING)
    private FixType fixType;

    @Enumerated(value = EnumType.STRING)
    private PcCheck pcCheck;

    private String etcMemo;

}
