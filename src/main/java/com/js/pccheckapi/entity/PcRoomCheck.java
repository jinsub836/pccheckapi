package com.js.pccheckapi.entity;

import com.js.pccheckapi.enums.FixType;
import com.js.pccheckapi.enums.PcCheck;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter

public class PcRoomCheck {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDate dateMaker;

    @Column(nullable = false)
    private Short computerNo;

    @Column(nullable = false, length = 20)
    private String update;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 25)
    private FixType fixType;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 20)
    private PcCheck pcCheck;

    @Column(columnDefinition = "TEXT")
    private String etcMemo;
}
