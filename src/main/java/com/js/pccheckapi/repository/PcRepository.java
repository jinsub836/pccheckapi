package com.js.pccheckapi.repository;

import com.js.pccheckapi.entity.PcRoomCheck;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PcRepository extends JpaRepository<PcRoomCheck,Long> {
}
