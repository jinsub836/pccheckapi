package com.js.pccheckapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcCheckApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcCheckApiApplication.class, args);
	}

}
