package com.js.pccheckapi.controller;

import com.js.pccheckapi.model.PcRequest;
import com.js.pccheckapi.model.PcResponse;
import com.js.pccheckapi.model.PcRoomItem;
import com.js.pccheckapi.service.PcService;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Response;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/pc")
public class PcController {
    private final PcService pcService;

    @PostMapping("/data")
    public String setPcRoom(@RequestBody PcRequest pcRequest){
        pcService.setPcRoom(pcRequest);
        return "입력이 완료되었습니다.";
    }

    @GetMapping
    public List<PcRoomItem> getPcRoom(){
        return pcService.getPcRoom();
    }

    @GetMapping("/detail/{id}")
    public PcResponse getPcInfo(@PathVariable long id){
        return  pcService.getPcInfo(id);
    }
}
